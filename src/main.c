#define _DEFAULT_SOURCE
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_HEAP_SIZE 10240
#define CLR_RED "\033[1;31m"
#define CLR_GREEN "\033[0;32m"
#define CLR_REGULAR "\033[0m"

extern void debug(const char *fmt, ...);

void debug_green(const char *msg){
    debug(CLR_GREEN);
    debug(msg);
    debug(CLR_REGULAR);
}

void err_red(const char *msg){
    debug(CLR_RED);
    err(msg);
    debug(CLR_REGULAR);
}

static void* test_heap_init() {
    debug("Heap initializing started\n");
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        err_red("Cannot initialize heap");
    }
    debug_green("OK. Heap initialized\n\n");
    return heap;
}

static inline struct block_header* get_block_by_allocated_data(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

static void allocate_pages_after_block(struct block_header *last_block) {
    void* test_addr = (uint8_t*) last_block + size_from_capacity(last_block->capacity).bytes;
    test_addr = (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() + (((size_t) test_addr % getpagesize()) > 0)));
    test_addr = map_pages(test_addr, 1024, MAP_FIXED_NOREPLACE);
}


// Выделение памяти
void test_normal_memory_allocation (struct block_header *first_block) {
    debug("Test 1 started:\n");

    const size_t test_size = 512;
    void *data1 = _malloc(test_size);
    if (data1 == NULL) {
        err_red("Test 1 failed: _malloc returned NULL\n");
    }

    debug_heap(stdout, first_block);

    if (first_block->is_free) {
        err_red("Test 1 failed: allocated_block isn't free\n");
    }
    if(first_block->capacity.bytes != test_size){
        err_red("Test 1 failed: allocated_block size isn't correct\n");
    }
    debug_green("OK. Test 1 passed\n\n");

    _free(data1);
}

// Освобождает один блок из выбранных
void free_block(struct block_header *first_block) {
    debug("Test 2 started:\n");

    void *data1 = _malloc(512), *data2 = _malloc(600);
    if (data1 == NULL || data2 == NULL) {
        err_red("Test 2 failed: _malloc returned NULL\n");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = get_block_by_allocated_data(data1), *data_block2 = get_block_by_allocated_data(data2);
    if (!data_block1->is_free) {
        err_red("Test 2 failed: free block is taken\n");
    }
    if (data_block2->is_free) {
        err_red("Test 2 failed: taken block is free\n");
    }

    debug_green("OK. Test 2 passed\n\n");
    _free(data1);
    _free(data2);
}

// Освобождение два блока из выбранных
void free_two_blocks(struct block_header *first_block) {
    debug("Test 3 started:\n");

    const size_t s1 = 512, s2 = 1024, s3 = 2048;
    void *data1 = _malloc(s1), *data2 = _malloc(s2), *data3 = _malloc(s3);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err_red("Test 3 failed: _malloc returned NULL\n");
    }
    // Сначала освобождим два блока подряд, а потом проверим, прошло ли объединение
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = get_block_by_allocated_data(data1), *data_block3 = get_block_by_allocated_data(data3);
    if (!data_block1->is_free) {
        err_red("Test 3 failed: free block is taken\n");
    }
    if (data_block3->is_free) {
        err_red("Test 3 failed: taken block is free\n");
    }
    if (data_block1->capacity.bytes != s1 + s2 + offsetof(struct block_header, contents)) {
        err_red("Test 3 failed: two free blocks didn't connect\n");
    }
    debug_green("OK. Test 3 passed\n\n");

    _free(data1);
    _free(data2);
    _free(data3);
}

void test_new_point_after_last(struct block_header *first_block) {
    debug("Test 4 started:\n");

    void *data1 = _malloc(INITIAL_HEAP_SIZE), *data2 = _malloc(INITIAL_HEAP_SIZE + 512), *data3 = _malloc(2048);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err_red("Test 4 failed: _malloc returned NULL\n");
    }
    _free(data3);
    _free(data2);

    debug_heap(stdout, first_block);

    struct block_header *data_block1 = get_block_by_allocated_data(data1), *data_block2 = get_block_by_allocated_data(data2);

    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        err_red("Test 4 failed: new region wasn't created after last\n");
    }
    debug_green("OK. Test 4 passed\n\n");

    _free(data1);
    _free(data2);
    _free(data3);
}

void test_new_point_at_new_place(struct block_header *first_block) {
    debug("Test 5 started:\n");

    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err_red("Test 5 failed: _malloc returned NULL\n");
    }

    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    allocate_pages_after_block(addr);
    void *data2 = _malloc(100000);

    debug_heap(stdout, first_block);

    struct block_header *data2_block = get_block_by_allocated_data(data2);
    if (data2_block == addr) {
        err_red("Test 5 failed: new block wasn't allocated at new place\n");
    }
    debug_green("OK. Test 5 passed\n\n");

    _free(data1);
    _free(data2);
}

int main(){
    struct block_header *first_block = (struct block_header*) test_heap_init();

    test_normal_memory_allocation(first_block);
    free_block(first_block);
    free_two_blocks(first_block);
    test_new_point_after_last(first_block);
    test_new_point_at_new_place(first_block);

    debug_green("OK: All tests passed\n\n");

    return 0;
}